import feedparser
import feedfinder
import re
import sys

# https://wiki.python.org/moin/RssLibraries
# Put the URLs to student feeds here. Note that some 
# engines do weird things with their RSS feeds... encourage
# your students to use Wordpress where possible.
urls = [
  
]

for url in urls:
  feed      = feedparser.parse( url )
  title     = feed[ "url" ]
  version   = feed["version"]
  urlcount  = 0
  postdates = []
  postcount = 0
  
  print "{0}: {1}".format(title, version)
  for counter, item in  enumerate(feed["entries"]):
    postcount += 1

    if "published" in item:
      postdates.append(item["published"])
    elif "date" in item:
      postdates.append(item["date"])
      
    if  "content" in item:
      content = item.content[0].value
    elif "description" in item:
      content = item.description
      
    wordlist = content.split()
    urlcount += len(wordlist)
    
  if postcount > 0:
    print "Posts: {0}".format(postcount)
    print "Words: {0}".format(urlcount)
    print "Words/Post: {0}".format(int(urlcount/postcount))
  
  for date in postdates:
    print " * {0}".format(date)
  print
  
